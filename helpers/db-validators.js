const { Employee } = require('../models');

const existEmail = async( correo = '') => {
    // Verificar si el correo existe
    const exist = await Employee.findOne({ correo });
    if (  exist ) {
        throw new Error(`El correo ${ correo } ya esta registrado`);
    }
    
}

const ExistIdEmployee = async( id ) => {
    // Verificar si el id existe
    const existId = await Employee.findById(id);
    if (  !existId ) {
        throw new Error(`El id ${ id } del empleado no existe`);
    }
    
}

const ExistIdentificationEmployee = async( type, data ) => {
    identification = data.req.body.numero_identificacion;
    // Verificar si el tipo de identificación y el numero existe
    const exist = await Employee.find({ tipo_identificacion: type, numero_identificacion: identification });

    if (  exist.length > 0 ) {
        throw new Error(`El tipo de identificación ${ type } y el numero ${ identification } del empleado ya existe`);
    }
    
}

const UpdateExistIdentificationEmployee = async( type, data ) => {
    id = data.req.params.id;
    identification = data.req.body.numero_identificacion;

    // verificar si el tipo de identificación y/o el numero de identificación cambio para poder hacer la validación
    const employee = await Employee.findById( id );
    if ( employee.tipo_identificacion !== type || employee.numero_identificacion !== identification ) {
        // Verificar si el tipo de identificación y el numero existe
        const exist = await Employee.find({ tipo_identificacion: type, numero_identificacion: identification });
    
        if (  exist.length > 0 ) {
            throw new Error(`El tipo de identificación ${ type } y el numero ${ identification } del empleado ya existe`);
        }        
    }
    
}

/**
 * Valida las colecciones permitidas
 */
const allowedCollections = ( collection = '' , collections = [] ) => {
    
    const incluida = collections.includes( collection );
    if ( !incluida ) {
        throw new Error(`La colección ${collection} no es permitida, [${collections}]`)
    }

    return true;
}

module.exports = {
    existEmail,
    ExistIdEmployee,
    ExistIdentificationEmployee,
    UpdateExistIdentificationEmployee,
    allowedCollections,
}