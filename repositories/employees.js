const Employee = require('../models/employee');

const validateEmail = async(pais_empleo, primer_nombre, primer_apellido, id = null) => {
    // Se construye el dominio
    const dominio = (pais_empleo === 'Colombia') ? `cidenet.com.co` : `cidenet.co.us`;
    // Se arma el email
    const firstName = primer_nombre.toLowerCase();
    const lastName  = primer_apellido.replace(/\s+/g, '').toLowerCase();
    let email       = `${firstName}.${lastName.trim()}@${dominio}`;

    // Buscamos si ya existe el email
    const query = { correo: email };
    const [ emailTotal ] = await Promise.all([
        Employee.countDocuments( query )
    ]);

    // Si ya existe se le agrega el secuencial ID
    if (emailTotal > 0) {
        // Para agregar el secuencial debemos buscar coincidencias con otros muy parecidos para asegurar no repetir el correo
        // Preguntamos si viene un id para excluir ese registro en la búsqueda
        let query = null;
        if ( id ) {
            query = { correo : { $regex: `${firstName}.${lastName.trim()}`, $options: 'i' }, _id: { $ne: id} };
        } else {
            query = { correo: { $regex: `${firstName}.${lastName.trim()}`, $options: 'i' } };
        }

        const [ total ] = await Promise.all([
            Employee.countDocuments( query )
        ]);
        
        console.log("total:", total);
        email = `${firstName}.${lastName}.${total}@${dominio}`;   
    }
    
    return email;
}

module.exports = {
    validateEmail,
}