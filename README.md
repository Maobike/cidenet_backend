# Notas:

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run start
```

### ### Compiles and hot-reloads for development with nodemon
```
npm run dev
```



RestServer

```
Servidor rest en NodeJS

Puerto manejado con paquete dotenv
Rutas manejadas con express

*** Búsquedas con condicionales
    En controllers/buscar.js

*** Carga de archivos y guardado en cloudinary
    En controllers/uploads.js

dependencies : 
  cors : "^2.8.5", para manejo de cors
  dotenv : "^8.2.0", para usar variables de entorno en archivo .env
  express : "^4.17.1", para fácil uso de servidor rest
  express-validator : "^6.10.0", para validar campos recibidos en el servicio rest
  mongoose : "^5.11.18", para conección con DB mongo
  uuid: "^8.3.2", generador de código uuid para identificador único de colección o tabla

    "cors": "^2.8.5",
    "dotenv": "^8.2.0",
    "express": "^4.17.1",
    "express-validator": "^6.10.0",
    "mongoose": "^5.11.18",
    "uuid": "^8.3.2"

```
