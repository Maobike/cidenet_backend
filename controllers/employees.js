const { response, request } = require('express');
const moment = require('moment');

const Employee = require('../models/employee');
const repositoriesEmployee = require('../repositories/employees');

const employeeGet = async(req = request, res = response) => {
    const id = req.params.id;

    const employee = await Promise.all([
        Employee.findById( id )
    ]);

    res.json({
        employee
    })
}

const employeesGet = async(req = request, res = response) => {

    const { limit = 5, from = 0 } = req.query;
    const query = { estado: true };

    const [ total, employees ] = await Promise.all([
        Employee.countDocuments( query ),
        Employee.find( query )
        .skip( Number( from ) )
        .limit( Number( limit ) )
    ]);

    res.json({
        total, employees
    })
}

const employeesPost = async(req, res = response) => {

    const {
        primer_apellido,
        segundo_apellido,
        primer_nombre,
        otros_nombres,
        pais_empleo,
        tipo_identificacion,
        numero_identificacion,
        fecha_ingreso,
        area
    } = req.body;

    // Armamos el correo
    const correo = await repositoriesEmployee.validateEmail(pais_empleo, primer_nombre, primer_apellido);

    const fecha_registro = moment().format('DD/MM/YYYY HH:mm:ss');
    
    const employee = new Employee( { 
        primer_apellido,
        segundo_apellido,
        primer_nombre,
        otros_nombres,
        pais_empleo,
        tipo_identificacion,
        numero_identificacion,
        correo,
        fecha_ingreso,
        area,
        fecha_registro
    } );

    // Guardar en DB
    await employee.save();

    res.json({
        employee
    })
}

const employeesPut = async(req, res = response) => {
    const id = req.params.id;
    const {
        _id,
        fecha_ingreso,
        estado,
        ...data
    } = req.body;

    const oldEmployee = await Employee.findById( id);

    if( oldEmployee.primer_nombre !== data.primer_nombre || oldEmployee.primer_apellido !== data.primer_apellido){
        // actualizamos el correo
        data.correo = await repositoriesEmployee.validateEmail(data.pais_empleo, data.primer_nombre, data.primer_apellido, id);
    }

    data.fecha_edicion = moment().format('DD/MM/YYYY HH:mm:ss');

    const employee = await Employee.findByIdAndUpdate( id, data, { new : true });

    res.json(employee);
}


module.exports = {
    employeeGet,
    employeesGet,
    employeesPost,
    employeesPut,
}