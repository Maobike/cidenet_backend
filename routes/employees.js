/**
 * api/employees
 */

const { Router, request } = require('express');
const { check } = require('express-validator');

const { validateFields, haveArea} = require('../middlewares');

const { ExistIdEmployee, ExistIdentificationEmployee, UpdateExistIdentificationEmployee } = require('../helpers/db-validators');

const { 
    employeeGet,
    employeesGet,
    employeesPost,
    employeesPut,
} = require('../controllers/employees');

const router = Router();

router.get('/:id', employeeGet);

router.get('/', employeesGet);

router.post('/', [
    check('primer_apellido', 'El primer apellido es obligatorio').not().isEmpty(),
    check('segundo_apellido', 'El segundo apellido es obligatorio').not().isEmpty(),
    check('primer_nombre', 'El primer nombre es obligatorio').not().isEmpty(),
    check('pais_empleo', 'No es un país válido').isIn(['Colombia','Estados Unidos']),
    check('tipo_identificacion', 'El tipo de identificación es obligatorio').not().isEmpty(),
    check('tipo_identificacion', 'No es un tipo de identificación válido').isIn(['Cédula de Ciudadanía', 'Cédula de Extranjería', 'Pasaporte', 'Permiso especial']),
    check('numero_identificacion', 'El número de identificación es obligatorio').not().isEmpty(),
    check('numero_identificacion', 'El número de identificación debe tener un máximo de 20 caracteres').isLength({ max: 20 }),
    check('fecha_ingreso', 'La fecha de ingreso es obligatoria').not().isEmpty(),
    check('area', 'El área es obligatoria').not().isEmpty(),
    check('area', 'No es un área válida').isIn(['Administración Financiera', 'Compras', 'Infraestructura', 'Desarrollo', 'Innovación', 'Operación', 'Talento Humano', 'Servicios Varios']),
    check('tipo_identificacion').custom( ExistIdentificationEmployee ),
    validateFields
], employeesPost);

router.put('/:id', [
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom( ExistIdEmployee ),
    check('primer_apellido', 'El primer apellido es obligatorio').not().isEmpty(),
    check('segundo_apellido', 'El segundo apellido es obligatorio').not().isEmpty(),
    check('primer_nombre', 'El primer nombre es obligatorio').not().isEmpty(),
    check('pais_empleo', 'No es un país válido').isIn(['Colombia','Estados Unidos']),
    check('tipo_identificacion', 'El tipo de identificación es obligatorio').not().isEmpty(),
    check('tipo_identificacion', 'No es un tipo de identificación válido').isIn(['Cédula de Ciudadanía', 'Cédula de Extranjería', 'Pasaporte', 'Permiso especial']),
    check('numero_identificacion', 'El número de identificación es obligatorio').not().isEmpty(),
    check('numero_identificacion', 'El número de identificación debe tener un máximo de 20 caracteres').isLength({ max: 20 }),
    check('fecha_ingreso', 'La fecha de ingreso es obligatoria').not().isEmpty(),
    check('area', 'El área es obligatoria').not().isEmpty(),
    check('area', 'No es un área válida').isIn(['Administración Financiera', 'Compras', 'Infraestructura', 'Desarrollo', 'Innovación', 'Operación', 'Talento Humano', 'Servicios Varios']),
    check('tipo_identificacion').custom( UpdateExistIdentificationEmployee ),
    validateFields
], employeesPut);

module.exports = router;