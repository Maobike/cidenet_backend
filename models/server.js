const express = require('express');
const cors = require('cors');
const { dbConnection } = require('../database/config');

class Server {

    constructor() {
        this.app = express()
        this.port = process.env.PORT;

        this.paths = {
            employees:   '/api/employees',
        }

        // Connect to DB
        this.connectDb();

        // MiddleWares
        this.middleWares();

        // Routes this App
        this.routes();
    }

    async connectDb(){
        await dbConnection();
    }

    middleWares() {

        // CORS
        this.app.use( cors() );

        // Public Dir
        this.app.use( express.static('public') );

        // Read and parse of the Body
        this.app.use( express.json() );

    }

    routes() {

        this.app.use( this.paths.employees, require('../routes/employees'));
    }

    listen() {
        this.app.listen( this.port, () => {
            console.log(`Server run in port: ${ this.port }`);
        });        
    }

}

module.exports = Server;
