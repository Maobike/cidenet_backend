
const Server = require('./server');
const Employee = require('./employee');

module.exports = {
    Server,
    Employee,
}