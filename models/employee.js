
const { Schema, model} = require('mongoose');

const EmployeeSchema = Schema({
    primer_apellido: {
        type : String,
        required: [true, 'El primer apellido es obligatorio']
    },
    segundo_apellido: {
        type : String,
        required: [true, 'El segundo apellido es obligatorio']
    },
    primer_nombre: {
        type : String,
        required: [true, 'El primer nombre es obligatorio']
    },
    otros_nombres: {
        type : String,
        required: false,
    },
    pais_empleo: {
        type : String,
        required: true,
        enum: ['Colombia', 'Estados Unidos']
    },
    tipo_identificacion: {
        type : String,
        required: true,
        enum: ['Cédula de Ciudadanía', 'Cédula de Extranjería', 'Pasaporte', 'Permiso especial']
    },
    numero_identificacion: {
        type : String,
        required: [true, 'El número de identificación es obligatorio']
    },
    correo: {
        type : String,
        required: true,
        unique: true
    },
    fecha_ingreso: {
        type : String,
        required: true,
    },
    area: {
        type : String,
        required: true,
        enum: ['Administración Financiera', 'Compras', 'Infraestructura', 'Desarrollo', 'Innovación', 'Operación', 'Talento Humano', 'Servicios Varios']
    },
    estado: {
        type : Boolean,
        default: true,
    },
    fecha_registro: {
        type : String,
        required: true,
    },
    fecha_edicion: {
        type : String,
        required: false,
    },
})

//EmployeeSchema.index({ tipo_identificacion: 1, numero_identificacion: 1 }, { unique: true });


// Esto saca los primeros parámetros del retorno JSON en la respuesta del endpoint.
EmployeeSchema.methods.toJSON = function() {
    const { __v, _id, ...employee  } = this.toObject();
    employee.uid = _id;
    return employee;
}

module.exports = model( 'Employee', EmployeeSchema );