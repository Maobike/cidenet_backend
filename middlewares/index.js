const validateFields = require('./validate-fields');
const validateAreas = require('./validate-areas');

module.exports = {
    ...validateFields,
    ...validateAreas,
}