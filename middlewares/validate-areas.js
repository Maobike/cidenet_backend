const { response, request, json } = require("express")

// ... Verify areas
const haveArea = ( ...areas ) => {
    return (req, res = response, next) => {

        console.table("aca:::::::"+req.areas);
        if ( !areas.includes( req.area )) {
            return res.status(401).json({
                msg: `El servicio requiere una de estas áreas ${ areas }`
            });
        }

        next();
    }
}


module.exports = {
    haveArea
}

